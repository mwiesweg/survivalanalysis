## Test environments
* local Ubuntu 18.04 install, R 3.6.3
* win-builder (devel)
* r-hub (Ubuntu+Fedora Linux, Windows Server)

## R CMD check results
There were no ERRORs or WARNINGs, or NOTEs (except the resubmission NOTE)

## Downstream dependencies
There are currently no downstream dependencies for this package.
